import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WatergraphPressureComponent } from './watergraph-pressure.component';

describe('WatergraphPressureComponent', () => {
  let component: WatergraphPressureComponent;
  let fixture: ComponentFixture<WatergraphPressureComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WatergraphPressureComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WatergraphPressureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
