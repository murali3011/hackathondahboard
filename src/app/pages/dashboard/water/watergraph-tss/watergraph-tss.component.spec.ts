import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WatergraphTssComponent } from './watergraph-tss.component';

describe('WatergraphTssComponent', () => {
  let component: WatergraphTssComponent;
  let fixture: ComponentFixture<WatergraphTssComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WatergraphTssComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WatergraphTssComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
