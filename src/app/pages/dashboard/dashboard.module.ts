import { NgModule } from '@angular/core';
import {
  NbActionsModule,
  NbButtonModule,
  NbCardModule,
  NbTabsetModule,
  NbUserModule,
  NbRadioModule,
  NbSelectModule,
  NbListModule,
  NbIconModule,
  NbAlertModule
} from '@nebular/theme';
import { NgxEchartsModule } from 'ngx-echarts';

import { ThemeModule } from '../../@theme/theme.module';
import { DashboardComponent } from './dashboard.component';
import { StatusCardComponent } from './status-card/status-card.component';
import { ContactsComponent } from './contacts/contacts.component';
import { RoomsComponent } from './rooms/rooms.component';
import { RoomSelectorComponent } from './rooms/room-selector/room-selector.component';
import { TemperatureComponent } from './temperature/temperature.component';
import { TemperatureDraggerComponent } from './temperature/temperature-dragger/temperature-dragger.component';
import { KittenComponent } from './kitten/kitten.component';
import { SecurityCamerasComponent } from './security-cameras/security-cameras.component';
import { ElectricityComponent } from './electricity/electricity.component';
import { ElectricityChartComponent } from './electricity/electricity-chart/electricity-chart.component';
import { WeatherComponent } from './weather/weather.component';
import { SolarComponent } from './solar/solar.component';
import { PlayerComponent } from './rooms/player/player.component';
import { TrafficComponent } from './traffic/traffic.component';
import { TrafficChartComponent } from './traffic/traffic-chart.component';
import { FormsModule } from '@angular/forms';
import { WaterComponent } from './water/water.component';
import { AirComponent } from './air/air.component';
import { SoilgraphComponent } from './temperature/soilgraph/soilgraph.component';
import { SoilgraphMoistComponent } from './temperature/soilgraph-moist/soilgraph-moist.component';
import { SoilgraphPhComponent } from './temperature/soilgraph-ph/soilgraph-ph.component';
import { AirgraphTempComponent } from './air/airgraph-temp/airgraph-temp.component';
import { AirgraphHumidityComponent } from './air/airgraph-humidity/airgraph-humidity.component';
import { WatergraphTdsComponent } from './water/watergraph-tds/watergraph-tds.component';
import { WatergraphPhComponent } from './water/watergraph-ph/watergraph-ph.component';
import { WatergraphTssComponent } from './water/watergraph-tss/watergraph-tss.component';
import { WatergraphSurfacetempComponent } from './water/watergraph-surfacetemp/watergraph-surfacetemp.component';
import { WatergraphPressureComponent } from './water/watergraph-pressure/watergraph-pressure.component';

@NgModule({
  imports: [
    FormsModule,
    ThemeModule,
    NbCardModule,
    NbUserModule,
    NbButtonModule,
    NbTabsetModule,
    NbActionsModule,
    NbRadioModule,
    NbSelectModule,
    NbListModule,
    NbIconModule,
    NbButtonModule,
    NgxEchartsModule,
    NbAlertModule
    
  ],
  declarations: [
    DashboardComponent,
    StatusCardComponent,
    TemperatureDraggerComponent,
    ContactsComponent,
    RoomSelectorComponent,
    TemperatureComponent,
    RoomsComponent,
    KittenComponent,
    SecurityCamerasComponent,
    ElectricityComponent,
    ElectricityChartComponent,
    WeatherComponent,
    PlayerComponent,
    SolarComponent,
    TrafficComponent,
    TrafficChartComponent,
    WaterComponent,
    AirComponent,
    SoilgraphComponent,
    SoilgraphMoistComponent,
    SoilgraphPhComponent,
    AirgraphTempComponent,
    AirgraphHumidityComponent,
    WatergraphTdsComponent,
    WatergraphPhComponent,
    WatergraphTssComponent,
    WatergraphSurfacetempComponent,
    WatergraphPressureComponent
    
    
  ],
})
export class DashboardModule { }
