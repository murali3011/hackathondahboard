import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WatergraphPhComponent } from './watergraph-ph.component';

describe('WatergraphPhComponent', () => {
  let component: WatergraphPhComponent;
  let fixture: ComponentFixture<WatergraphPhComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WatergraphPhComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WatergraphPhComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
