import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AirgraphHumidityComponent } from './airgraph-humidity.component';

describe('AirgraphHumidityComponent', () => {
  let component: AirgraphHumidityComponent;
  let fixture: ComponentFixture<AirgraphHumidityComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AirgraphHumidityComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AirgraphHumidityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
