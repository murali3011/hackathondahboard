import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WatergraphSurfacetempComponent } from './watergraph-surfacetemp.component';

describe('WatergraphSurfacetempComponent', () => {
  let component: WatergraphSurfacetempComponent;
  let fixture: ComponentFixture<WatergraphSurfacetempComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WatergraphSurfacetempComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WatergraphSurfacetempComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
