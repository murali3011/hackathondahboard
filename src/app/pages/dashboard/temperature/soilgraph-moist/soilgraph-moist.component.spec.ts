import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SoilgraphMoistComponent } from './soilgraph-moist.component';

describe('SoilgraphMoistComponent', () => {
  let component: SoilgraphMoistComponent;
  let fixture: ComponentFixture<SoilgraphMoistComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SoilgraphMoistComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SoilgraphMoistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
