import { Injectable } from '@angular/core';
import { of as observableOf, Observable } from 'rxjs';
import { Camera, SecurityCamerasData } from '../data/security-cameras';

@Injectable()
export class SecurityCamerasService extends SecurityCamerasData {

  private cameras: Camera[] = [
    {
      title: 'Tomato-Farm',
      source: 'assets/camera/Tomatofarm1.jpg',
    },
    {
      title: 'GreenChilli-Farm',
      source: 'assets/camera/GreenchillyFarm.jpg',
    },
    {
      title: 'Purple Sessile Joyweed -Farm',
      source: 'assets/camera/LettuceFarm.jpg',
    },
    {
      title: 'MintPlant-Farm',
      source: 'assets/camera/MintFarm.jpg',
    },
  ];

  getCamerasData(): Observable<Camera[]> {
    return observableOf(this.cameras);
  }
}
