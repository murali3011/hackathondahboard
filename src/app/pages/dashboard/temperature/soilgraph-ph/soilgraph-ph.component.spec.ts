import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SoilgraphPhComponent } from './soilgraph-ph.component';

describe('SoilgraphPhComponent', () => {
  let component: SoilgraphPhComponent;
  let fixture: ComponentFixture<SoilgraphPhComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SoilgraphPhComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SoilgraphPhComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
