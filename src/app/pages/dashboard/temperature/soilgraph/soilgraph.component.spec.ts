import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SoilgraphComponent } from './soilgraph.component';

describe('SoilgraphComponent', () => {
  let component: SoilgraphComponent;
  let fixture: ComponentFixture<SoilgraphComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SoilgraphComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SoilgraphComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
