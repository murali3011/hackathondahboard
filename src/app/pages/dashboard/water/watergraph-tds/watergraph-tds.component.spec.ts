import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WatergraphTdsComponent } from './watergraph-tds.component';

describe('WatergraphTdsComponent', () => {
  let component: WatergraphTdsComponent;
  let fixture: ComponentFixture<WatergraphTdsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WatergraphTdsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WatergraphTdsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
