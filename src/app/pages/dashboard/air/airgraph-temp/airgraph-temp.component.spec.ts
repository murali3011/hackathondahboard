import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AirgraphTempComponent } from './airgraph-temp.component';

describe('AirgraphTempComponent', () => {
  let component: AirgraphTempComponent;
  let fixture: ComponentFixture<AirgraphTempComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AirgraphTempComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AirgraphTempComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
